#include <stdio.h>

int  main (){

    long int   var_long_int; //variable tipo numero entero largo
    char       var_char;    // variable tipo caracter con variables de tipo char no se puede usar long

    int        var_int;     // variable tipo numero entero
    float      var_float;
    double     var_double;
    long long int var_2_long_int; //2 long insta al compilador a hacer la variable dos veces el doble mas grande, puede que no funcione dependiendo del compilador
    

    short int       var_short_int; // no se puede hacer dos short, 1 como maximo, no se pueden juntar long y short en la misma variable


    printf ("variable de tipo long int ocupa: %lu bytes\n", sizeof (var_long_int) ); 
  
    printf (" variable de tipo int ocupa: %lu bytes\n", sizeof (var_int) );
  
    printf (" variable de tipo char ocupa: %lu bytes\n", sizeof (var_char) );
 
    printf (" variable de tipo double ocupa: %lu bytes\n", sizeof (var_double) ); 

    printf (" variable de tipo float ocupa: %lu bytes\n", sizeof (var_float) );
 
    printf (" variable de tipo long 2int ocupa:  %lu bytes\n", sizeof (var_2_long_int) );     
 
    printf (" variable de tipo short int ocupa : %lu bytes\n", sizeof (var_short_int) ); 
   

    
    
    
    
    
     return 0;

}
