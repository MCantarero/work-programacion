#include <stdio.h>
#include<stdlib.h>
#define DIM 3

double* SUMA(double dim1[DIM], double dim2[DIM]){
    static  double  sum[DIM];

    for(int i=0; i<DIM ;i++)
        sum[i] = dim1[i]+dim2[i];

    return sum;
}

void leer(double *vector){

    for(int i=0; i<DIM ;i++)
        printf("%.1lf ",vector[i]);

    printf("\n");

}

void ask_vector(double v[DIM]){

    char dimensiones[DIM]{'x','y','z'};

    for(int i=0; i<DIM ; i++){
        printf("Dime la coordenada %c: ",dimensiones[i]);

        scanf("%lf",&v[i]);

        printf("\r");
    }

    system("clear");


}

int main (int argc, char *argy[]){

    double *suma;
    double vector1[DIM], vector2[DIM];

    ask_vector(vector1);
    ask_vector(vector2);
    printf("1º vector \n");
    leer(vector1);
    printf("2º vector \n");
    leer(vector2);
    printf("la suma de los dos es:  ");
   
    suma = SUMA(vector1,vector2);

    for(int i=0; i<DIM; i++)
        printf("%.1lf ",suma[i]);

    printf("\n");

    return EXIT_SUCCESS;
}
