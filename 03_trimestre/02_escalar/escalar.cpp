#include <stdio.h>
#include<stdlib.h>
#define DIM 3

void askVector(double vector[DIM]){

    char letra[DIM]{'X','Y','Z'};

    for( int i=0 ; i<DIM ; i++ ){
        printf("Dime la %c  del vector: ",letra[i]);
        scanf("%lf",&vector[i]);

    }
}

double escalar(double *vector1,double *vector2){

    double resultado=0;
    for(int i=0; i<DIM;i++)
        resultado+=(vector1[i])*(vector2[i]);

    return resultado;
} 



int main (int argc, char *argy[]){

    double vector1[DIM];
    double vector2[DIM];
    printf("VECTOR 1");
    printf("\n");
    askVector(vector1);
    printf("\n");
    printf("VECTOR 2");
    printf("\n");
    askVector(vector2); 
    printf("\n");
    double resultado=escalar(vector1,vector2);
    printf("EL PRODUCTO ESCALAR ES:");
    printf("%.1lf ",resultado);
   /* for(int i=0; i<DIM; i++)
        printf("%lf ",vector1[i]);
        
     */  

    return EXIT_SUCCESS;
}
