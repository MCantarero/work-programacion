#include<stdio.h>
#include<stdlib.h>
#define SIZE 3


int main (int argc, char *argy[]){

    double Matriz[SIZE][SIZE]{

            1,2,4,
            6,7,8,
            9,3,10    
    };

    double Multiplicacion=1,
           DeterminanteParte1=0,
           DeterminanteParte2=0,
           Resultado=0;


    for(int i=0;i<SIZE;i++){
        Multiplicacion=1;
        for(int a=0;a<SIZE;a++){
            printf("%.1lf ",Matriz[(a+i)%SIZE][a]);
            Multiplicacion*=Matriz[(a+i)%SIZE][a];
        }
        DeterminanteParte1+=Multiplicacion;
        printf("\n");
    }


    printf("\n\n");

    for(int i=0;i<SIZE;i++){
        Multiplicacion=1;
        for(int a=SIZE;a>0;a--){
            printf("%.1lf ",Matriz[(SIZE+i-a)%SIZE][a-1]);
            Multiplicacion*=Matriz[(SIZE+i-a)%SIZE][a-1];
        }
        DeterminanteParte2+=Multiplicacion;
        printf("\n");
    }

    printf("\n\n");
    Resultado=DeterminanteParte1-DeterminanteParte2;
    printf("El determinante de esa Matriz es: %.1lf",Resultado);






    return EXIT_SUCCESS;
}
