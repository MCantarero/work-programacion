#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct TEmpleado{

    char *nombre;
    char *apellido1;
    char *apellido2;
    unsigned edad;
    double salario;

};

void NData(struct TEmpleado *NEmpleado){

    printf("Escriba nombre del  Empleado: ");
    scanf("%ms", &NEmpleado->nombre);
    printf("\n");
    printf("Escriba el 1º Apellido del Empleado: ");
    scanf("%ms", &NEmpleado->apellido1);
    printf("\n");
    printf("Escriba el 2º Apellido del Empleado: ");
    scanf("%ms", &NEmpleado->apellido2);
    printf("\n");
    printf("Engrese la Edad del Empleado: ");
    scanf("%u", &NEmpleado->edad);
    printf("\n");
    printf("Introduzca el Salario del Empleado: ");
    scanf("%lf", &NEmpleado->salario);
    printf("\n");

}
/*
struct TEmpleado  EmpleadoMesPorValor(struct TEmpleado EmpleadoMes){

    struct TEmpleado buffer;
    printf("¿Cual es el Empleado del mes?\n");
    printf("Nombre: ");
    scanf("%ms", &buffer.nombre);


    EmpleadoMes.nombre = buffer.nombre;


    printf("\n");

    return EmpleadoMes;

}
*/

void EmpleadoMesPorReferencia(struct TEmpleado *EmpleadoMes,struct TEmpleado Candidato1, struct TEmpleado Candidato2){

    struct TEmpleado buffer;
    printf("¿Cual es el Empleado del mes?\n");
    printf("Nombre: ");
    scanf("%ms", &buffer.nombre);

    if( strcmp(buffer.nombre, Candidato1.nombre) == 0)
        EmpleadoMes = &Candidato1;
    if( strcmp(buffer.nombre, Candidato2.nombre) == 0)
        EmpleadoMes = &Candidato2;
        else
            printf("No existe el empleado");

}




int main (int argc, char *argy[]){

    struct TEmpleado Empleado1;
    struct TEmpleado Empleado2;
    struct TEmpleado *EmpleadoMes;

    NData( &Empleado1 );
    NData( &Empleado2 );
    //EmpleadoMesPorValor(*EmpleadoMes);
    EmpleadoMesPorReferencia(EmpleadoMes,Empleado1,Empleado2);







    return EXIT_SUCCESS;
}
